//
//  MFButtonViewControllerTests.m
//  ExampleApp
//
//  Created by Matthew Flint on 19/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MFButtonViewController.h"
#import "MFButtonServiceProtocol.h"
#import "MFObjectFactory+TestExtensions.h"

#define C1AssertActionTouchUpInside(view, target, selectorName, format...) \
XCTAssert([[view actionsForTarget:target forControlEvent:UIControlEventTouchUpInside] containsObject:selectorName], format);

@interface MFButtonViewControllerTests : XCTestCase
@end

@implementation MFButtonViewControllerTests
{
    id<MFButtonServiceProtocol> mockService;
    MFButtonViewController *sut;
}

-(void)setUp {
    [super setUp];
    
    mockService = OCMProtocolMock(@protocol(MFButtonServiceProtocol));
    [[MFObjectFactory sharedInstance] addOverride:mockService forKey:kButtonService];
    
    UIStoryboard *storyboard =[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    sut = [storyboard instantiateViewControllerWithIdentifier:@"ButtonViewController"];
    [sut view];
}

-(void)tearDown {
    [[MFObjectFactory sharedInstance] reset];
    [super tearDown];
}

-(void)testThatTheLabelOutletIsConnected {
    XCTAssertNotNil(sut.resultLabel);
}

-(void)testThatTheButtonOutletIsConnected {
    XCTAssertNotNil(sut.button);
}

-(void)testThatTheButtonActionIsConnected {
    C1AssertActionTouchUpInside(sut.button, sut, @"onButtonPressed:");
}

-(void)testThatTheButtonActionProdsTheService {
    [sut onButtonPressed:nil];
    
    OCMVerify([mockService serviceAction]);
}

@end
