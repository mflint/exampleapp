//
//  MFTableViewControllerTests.m
//  ExampleApp
//
//  Created by Matthew Flint on 20/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MFTableViewController.h"
#import "MFObjectFactory+TestExtensions.h"
#import "MFTableViewDataSourceService.h"

@interface MFTableViewControllerTests : XCTestCase

@end

@implementation MFTableViewControllerTests
{
    id<UITableViewDataSource> tableViewDataSourceMock;
    MFTableViewController *sut;
}

- (void)setUp
{
    [super setUp];
    
    tableViewDataSourceMock = OCMProtocolMock(@protocol(UITableViewDataSource));
    [[MFObjectFactory sharedInstance] addOverride:tableViewDataSourceMock forKey:kTableDataSource];
    
    UIStoryboard *storyboard =[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    sut = [storyboard instantiateViewControllerWithIdentifier:@"TableViewController"];
    [sut view];
}

-(void)tearDown {
    [[MFObjectFactory sharedInstance] reset];
    [super tearDown];
}

- (void)testThatTheTableViewIsConnected
{
    XCTAssertNotNil(sut.tableView);
}

- (void)testThatTheTableViewDataSourceIsConnected
{
    XCTAssertEqual(sut.tableView.dataSource, tableViewDataSourceMock);
}

- (void)testThatTheCellIsRegistered
{
    XCTAssertNotNil([sut.tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifier]);
}

@end
