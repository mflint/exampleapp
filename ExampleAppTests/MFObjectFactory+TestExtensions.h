//
//  MFObjectFactory+TestExtensions.h
//  ExampleApp
//
//  Created by Matthew Flint on 19/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import "MFObjectFactory.h"

@interface MFObjectFactory (TestExtensions)

- (void)addOverride:(id)service forKey:(NSString *)key;
- (void)reset;

@end
