//
//  MFObjectFactoryTests.m
//  ExampleApp
//
//  Created by Matthew Flint on 20/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MFObjectFactory+TestExtensions.h"

#import "MFButtonService.h"
#import "MFTableViewDataSourceService.h"

@interface MFObjectFactoryTests : XCTestCase
@end

@implementation MFObjectFactoryTests
{
    MFObjectFactory *sut;
}

- (void)setUp
{
    [super setUp];
    sut = [MFObjectFactory sharedInstance];
}

- (void)tearDown
{
    [sut reset];
    [super tearDown];
}

- (void)testDefaultButtonServiceImplementation
{
    XCTAssertTrue([[sut getService:kButtonService] isKindOfClass:[MFButtonService class]]);
}

- (void)testDefaultTableViewDataSourceServiceImplementation
{
    XCTAssertTrue([[sut getService:kTableDataSource] isKindOfClass:[MFTableViewDataSourceService class]]);
}

@end
