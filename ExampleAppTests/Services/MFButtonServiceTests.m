//
//  MFButtonServiceTests.m
//  ExampleAppTests
//
//  Created by Matthew Flint on 18/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MFButtonService.h"

@interface MFButtonServiceTests : XCTestCase
@end

@implementation MFButtonServiceTests
{
    id<MFButtonServiceDelegate> mockDelegate;
    MFButtonService *sut;
}

- (void)setUp
{
    [super setUp];
    
    mockDelegate = OCMProtocolMock(@protocol(MFButtonServiceDelegate));
    
    sut = [[MFButtonService alloc] init];
    sut.delegate = mockDelegate;
}

- (void)testExampleService
{
    [sut serviceAction];
    
    OCMVerify([mockDelegate someOperationSucceeded]);
}

@end
