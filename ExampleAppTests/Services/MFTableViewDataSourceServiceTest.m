//
//  MFTableViewDataSourceServiceTest.m
//  ExampleApp
//
//  Created by Matthew Flint on 20/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>
#import <OCMock/OCMock.h>

#import "MFTableViewDataSourceService.h"

@interface MFTableViewDataSourceServiceTest : XCTestCase
@end

@implementation MFTableViewDataSourceServiceTest
{
    MFTableViewDataSourceService *sut;
    UITableView *mockTableView;
}

- (void)setUp
{
    [super setUp];
    sut = [[MFTableViewDataSourceService alloc] init];
    
    mockTableView = OCMClassMock([UITableView class]);
}

- (void)testSectionCount
{
    XCTAssertEqual([sut numberOfSectionsInTableView:nil], 1);
}

- (void)testRowCount
{
    XCTAssertEqual([sut tableView:nil numberOfRowsInSection:0], 4);
}

- (void)testCellsForRows
{
    // mock UITabelView must return a skeleton UITableViewCell
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    OCMStub([mockTableView dequeueReusableCellWithIdentifier:kCellReuseIdentifier forIndexPath:OCMOCK_ANY]).
    andReturn(cell);
    
    UITableViewCell *cell0 = [sut tableView:mockTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    XCTAssertEqualObjects(cell0.textLabel.text, @"Hey");
    
    UITableViewCell *cell1 = [sut tableView:mockTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    XCTAssertEqualObjects(cell1.textLabel.text, @"Ho");
    
    UITableViewCell *cell2 = [sut tableView:mockTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    XCTAssertEqualObjects(cell2.textLabel.text, @"Let's");
    
    UITableViewCell *cell3 = [sut tableView:mockTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    XCTAssertEqualObjects(cell3.textLabel.text, @"Go");
}

@end
