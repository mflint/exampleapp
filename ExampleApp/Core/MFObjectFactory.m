//
//  MFObjectFactory.m
//  ExampleApp
//
//  Created by Matthew Flint on 18/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import "MFObjectFactory.h"
#import "MFButtonService.h"
#import "MFTableViewDataSourceService.h"

// service keys
NSString *const kButtonService           = @"kButtonService";
NSString *const kTableDataSource         = @"kTableDataSource";

@interface MFObjectFactory ()
@property(nonatomic, strong) NSMutableDictionary *overrides;
@end

@implementation MFObjectFactory

+ (instancetype)sharedInstance {
    static MFObjectFactory *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[MFObjectFactory alloc] init];
    });
    
    return _sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        self.overrides = [NSMutableDictionary dictionary];
    }
    
    return self;
}

- (void)addOverride:(id)service forKey:(NSString *)key {
    self.overrides[key] = service;
}

- (void)reset {
    [self.overrides removeAllObjects];
}

- (id)getService:(NSString *)key {
    id result = self.overrides[key];
    
    if (!result) {
        if ([kButtonService isEqualToString:key]) {
            result = [[MFButtonService alloc] init];
        } else if ([kTableDataSource isEqualToString:key]) {
            result = [[MFTableViewDataSourceService alloc] init];
        }
    }
    
    if (!result) {
        NSAssert(result, @"MFObjectFactory: no implementation found for key %@", key);
    }
    
    return result;
}

@end
