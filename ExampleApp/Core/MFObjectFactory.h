//
//  MFObjectFactory.h
//  ExampleApp
//
//  Created by Matthew Flint on 18/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kButtonService;
extern NSString *const kTableDataSource;

@interface MFObjectFactory : NSObject

+ (instancetype)sharedInstance;

- (id)getService:(NSString *)key;

@end
