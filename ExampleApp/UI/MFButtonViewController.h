//
//  MFButtonViewController.h
//  ExampleApp
//
//  Created by Matthew Flint on 18/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFButtonServiceDelegate.h"

@interface MFButtonViewController : UIViewController<MFButtonServiceDelegate>

@property(weak) IBOutlet UILabel *resultLabel;
@property(weak) IBOutlet UIButton *button;

-(IBAction)onButtonPressed:(id)sender;

@end
