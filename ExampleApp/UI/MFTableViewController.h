//
//  MFTableViewController.h
//  ExampleApp
//
//  Created by Matthew Flint on 20/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MFTableViewController : UIViewController

@property(weak) IBOutlet UITableView *tableView;
@end
