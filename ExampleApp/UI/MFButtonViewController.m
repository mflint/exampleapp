//
//  MFButtonViewController.m
//  ExampleApp
//
//  Created by Matthew Flint on 18/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import "MFButtonViewController.h"
#import "MFObjectFactory.h"
#import "MFButtonService.h"

@implementation MFButtonViewController
{
    MFButtonService *buttonService;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // get the service from the object factory
    buttonService = [[MFObjectFactory sharedInstance] getService:kButtonService];
    buttonService.delegate = self;
}

- (void)onButtonPressed:(id)sender {
    // our VC is stupid, so get the service to do the work
    [buttonService serviceAction];
}

- (void)someOperationSucceeded {
    self.resultLabel.text = @"Well, that worked";
}

- (void)someOperationFailedWithError:(NSString *)error {
    
}

@end
