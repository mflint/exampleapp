//
//  MFTableViewController.m
//  ExampleApp
//
//  Created by Matthew Flint on 20/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import "MFTableViewController.h"
#import "MFObjectFactory.h"
#import "MFTableViewDataSourceService.h"

@interface MFTableViewController ()

@end

@implementation MFTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // get table things from object factory
    self.tableView.dataSource = [[MFObjectFactory sharedInstance] getService:kTableDataSource];
    
    // register the table view cell
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellReuseIdentifier];
}

@end
