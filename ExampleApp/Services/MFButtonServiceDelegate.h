//
//  MFButtonServiceDelegate.h
//  ExampleApp
//
//  Created by Matthew Flint on 18/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MFButtonServiceDelegate <NSObject>

- (void)someOperationSucceeded;
- (void)someOperationFailedWithError:(NSString *)error;

@end
