//
//  MFButtonService.m
//  ExampleApp
//
//  Created by Matthew Flint on 18/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import "MFButtonService.h"

@implementation MFButtonService
@synthesize delegate;

- (id)init {
    if (self = [super init]) {
        //
    }
    
    return self;
}

- (void)serviceAction {
    [self.delegate someOperationSucceeded];
}

@end
