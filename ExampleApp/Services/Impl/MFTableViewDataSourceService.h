//
//  MFTableViewDataSourceService.h
//  ExampleApp
//
//  Created by Matthew Flint on 20/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kCellReuseIdentifier;

@interface MFTableViewDataSourceService : NSObject <UITableViewDataSource>

@end
