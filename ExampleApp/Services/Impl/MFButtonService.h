//
//  MFButtonService.h
//  ExampleApp
//
//  Created by Matthew Flint on 18/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MFButtonServiceProtocol.h"
#import "MFButtonServiceDelegate.h"

@interface MFButtonService : NSObject<MFButtonServiceProtocol>

@end
