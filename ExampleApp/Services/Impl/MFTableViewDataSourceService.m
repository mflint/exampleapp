//
//  MFTableViewDataSourceService.m
//  ExampleApp
//
//  Created by Matthew Flint on 20/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import "MFTableViewDataSourceService.h"

NSString *const kCellReuseIdentifier = @"kCellReuseIdentifier";

@implementation MFTableViewDataSourceService

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifier forIndexPath:indexPath];
    NSString *text = nil;
    
    switch(indexPath.row) {
        case 0:
            text = @"Hey";
            break;
        case 1:
            text = @"Ho";
            break;
        case 2:
            text = @"Let's";
            break;
        case 3:
            text = @"Go";
            break;
        default:
            break;
    }
    
    cell.textLabel.text = text;
    return cell;
}

@end
