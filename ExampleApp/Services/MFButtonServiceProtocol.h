//
//  MFButtonServiceProtocol.h
//  ExampleApp
//
//  Created by Matthew Flint on 20/07/2014.
//  Copyright (c) 2014 Green Light. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MFButtonServiceDelegate.h"

@protocol MFButtonServiceProtocol <NSObject>

@property(weak) id<MFButtonServiceDelegate> delegate;

- (void)serviceAction;

@end
